<?php

App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');

class FollowersController extends AppController {
    public $uses = ['User', 'Tweet', 'Comment', 'Like', 'Follower'];
    public $recursive = 1;
    public $components = ['Paginator', 'RequestHandler'];
    public $helpers = array('Html', 'Form', 'Js');

    public function follow() {
        $this->request->onlyAllow('ajax');
        $this->autoRender = false;

        $post = $this->request->data;

        $find = $this->Follower->find('first', [
            'conditions' => [
                'AND' => [
                    ['Follower.user_id' => $post['user_id']],
                    ['Follower.following_id' => $post['following_id']]
                ]
        ]]);

        if ($this->RequestHandler->isAjax()) {
            if(empty($find)) {
                $this->Follower->create();
                $data = [
                    'user_id' => $post['user_id'],
                    'following_id' => $post['following_id'],
                    'status' => 1
                ];
            } else {
                $data = [
                    'id' => $find['Follower']['id'],
                    'user_id' => $post['user_id'],
                    'following_id' => $post['following_id'],
                    'status' => 1
                ];
            }

            if ($this->Follower->save($data)) {
                $message = [
                    'message' => 'Follow Successful',
                    'status' => 'success'
                ];
                return json_encode($message);
            } else {
                $message = [
                    'message' => 'Follow Failed',
                    'status' => 'success'
                ];
                return json_encode($message);
            }
        }
    }

    public function unfollow() {
        $this->request->onlyAllow('ajax');
        $this->loadModel('Follower');
        $this->autoRender = false;

        $post = $this->request->data;

        $find = $this->Follower->find('first', [
            'conditions' => [
                'AND' => [
                    ['Follower.following_id' => $post['following_id']],
                ]
        ]]);

        if ($this->RequestHandler->isAjax()){
            $data = [
                'id' => $find['Follower']['id'],
                'user_id' => $post['user_id'],
                'following_id' => $post['following_id'],
                'status' => 0
            ];

            if ($this->Follower->save($data)) {
                $message = [
                    'message' => 'Like Successful',
                    'status' => 'success'
                ];
                return json_encode($message);
            } else {
                $message = [
                    'message' => 'Like Failed',
                    'status' => 'success'
                ];
                return json_encode($message);
            }
        }
    }
}