<?php

App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');

class LikesController extends AppController {
    public $uses = ['User', 'Tweet', 'Comment', 'Like', 'Follower'];
    public $recursive = 1;
    public $components = ['Paginator', 'RequestHandler'];
    public $helpers = array('Html', 'Form', 'Js');

    public function like() {
        $this->request->onlyAllow('ajax');
        $this->autoRender = false;
        $this->loadModel('Like');
        $this->loadModel('Tweet');

        $post = $this->request->data;

        $find = $this->Like->find('first', [
            'conditions' => [
                'AND' => [
                    ['Like.tweet_id' => $post['tweet_id']],
                    ['Like.user_id' => $post['user_id']]
                ]
        ]]);
        if ($this->RequestHandler->isAjax()) {
            if(empty($find)) {
                $this->Like->create();
                $data = [
                    'tweet_id' => $post['tweet_id'],
                    'user_id' => $post['user_id'],
                    'is_liked' => 1
                ];
            } else {
                $data = [
                    'id' => $find['Like']['id'],
                    'tweet_id' => $post['tweet_id'],
                    'user_id' => $post['user_id'],
                    'is_liked' => 1

                ];
            }

            if ($this->Like->save($data)) {
                $message = [
                    'data_count' => $this->Like->find('count', [
                        'conditions' => [
                            'Like.tweet_id' => $post['tweet_id'],
                             'is_liked' => 1
                            ]
                        ]),
                    'message' => 'Like Successful',
                    'status' => 'success'
                ];
                return json_encode($message);
            } else {
                $message = [
                    'message' => 'Like Failed',
                    'status' => 'success'
                ];
                return json_encode($message);
            }
        }
    }

    public function unlike() {
        $this->request->onlyAllow('ajax');
        $this->loadModel('Like');
        $this->loadModel('Tweet');
        $this->autoRender = false;

        $post = $this->request->data;

        $find = $this->Like->find('first', [
            'conditions' => [
                'AND' => [
                    ['Like.tweet_id' => $post['tweet_id']],
                ]
        ]]);

        if ($this->RequestHandler->isAjax()){
            $data = [
                'id' => $find['Like']['id'],
                'tweet_id' => $post['tweet_id'],
                'user_id' => $post['user_id'],
                'is_liked' => 0
            ];

            if ($this->Like->save($data)) {
                $message = [
                    'data_count' => $this->Like->find('count', [
                        'conditions' => [
                            'Like.tweet_id' => $post['tweet_id'],
                            'is_liked' => 1
                            ]
                        ]
                    ),
                    'message' => 'Unlike Successful',
                    'status' => 'success'
                ];
                return json_encode($message);
            } else {
                $message = [
                    'message' => 'Unlike Failed',
                    'status' => 'failed'
                ];
                return json_encode($message);
            }
        }
    }

}