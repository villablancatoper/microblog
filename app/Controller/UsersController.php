<?php

App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');

class UsersController extends AppController {
    public $uses = ['User', 'Tweet', 'Comment', 'Like', 'Follower'];
    public $recursive = 0;
    public $components = ['Paginator', 'RequestHandler'];
    public $helpers = array('Html', 'Form', 'Js');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'logout', 'register', 'activate');

        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    public function login() {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($this->Auth->login()) {
                $this->Session->write('username', $data['User']['username']);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function dashboard() {
        $this->layout = 'main';


        //finds user information
        $User = $this->User->find('first', [
            'conditions' => [
                'User.username' => $this->Session->read('username')
            ]
        ]);
        $this->set(compact('User'));

        $try = $this->Follower->find('list', [
            'fields' => ['Follower.following_id'],
            'recursive' => -1,
            'group' => ['Follower.following_id'],
            'conditions' => [
                'Follower.user_id' => $this->Session->read('Auth.User.id')
            ]
        ]);

        $likeList = $this->Like->find('list', [
            'fields' => ['Like.tweet_id'],
            'recursive' => -1,
            'group' => ['Like.tweet_id'],
        ]);


        $id = $User['User']['id'];

        $Follower = $this->Follower->find('all', array(
        'conditions' => [
            'AND' => [
                'Follower.user_id' => $this->Auth->User('id'),
                'Follower.status' => 1
            ]
        ]
        ));
        $this->set(compact('Follower'));


        $userTweet = $this->Tweet->find('all', 
            array(
            'order' => ['Tweet.created' => 'desc'],
            'conditions' => [
                'OR' => [
                    ['Tweet.user_id' => $try],
                    ['Tweet.user_id' => $this->Session->read('Auth.User.id')]
                ],
                'Tweet.deleted' => 0
            ]
        ));
        $this->set(compact('userTweet'));


        $commentTweet = $this->Comment->find('all', array('joins' => array(
                array(
                    'table' => 'tweets',
                    'alias' => 'tweetJoin',
                    'type' => 'INNER',
                    'conditions' => array(
                        ['Comment.tweet_id = tweetJoin.id'],
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'userJoin',
                    'type' => 'INNER',
                    'conditions' => array(
                        ['Comment.user_id = userJoin.id'],
                    )
                )
            ),
        'order' => ['Comment.created' => 'desc']
        ));
        $this->set(compact('commentTweet'));

        $count = $this->Like->find('all', array(
            'conditions' => [
                'OR' => [
                    ['Like.is_liked' => 1]
                ]
            ]
        ));
        $this->set(compact('count'));

        //pr($count); die;

        $isLiked = $this->Like->find('all', array(
            'conditions' => [
                'AND' => [
                    'Like.user_id' => $this->Auth->User('id'),
                    'Like.is_liked' => 1
                ]
            ]
        ));
        $this->set(compact('isLiked'));

        //pr($isLiked); die;

        $isFollowed = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.following_id' => $this->Auth->User('id'),
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowed'));

        $isFollowing = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.user_id' => $this->Auth->User('id'),
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowing'));

        
        if ($this->request->is('post')) {
            if (isset($this->request->data['Tweet'])) {

                $filename = '';

                if (!empty($this->request->data['Tweet']['picture']) && !empty($this->request->data['Tweet']['picture']['tmp_name'])) {

                    $uploadData = $this->request->data['Tweet']['picture']['tmp_name'];

                    $filename = basename($this->request->data['Tweet']['picture']['name']);
                    $uploadFolder = WWW_ROOT. 'img' ;
                    $filename = time() . '_' . $filename;
                    $uploadPath =  $uploadFolder . DS . 'users' . DS .  $filename;
    
    
                    if (!move_uploaded_file($uploadData, $uploadPath)) {
                        $this->Flash->error(__('Error Uploading Image, Please try again'));
                    }
                    $this->request->data['Tweet']['picture'] = $filename;
                } else {
                    unset($this->request->data['Tweet']['picture']);
                } 

                $data = $this->request->data;
                $this->Tweet->set($data);
                if ($this->Tweet->save($data)) {
                    return $this->redirect(array('action' => 'dashboard'));
                }
                unset($data['Tweet']['content']);
            }
            if (isset($this->request->data['Comment'])) {
                $data = $this->request->data;
                $this->Comment->set($data);
                if ($this->Comment->save($data)) {
                    return $this->redirect(array('action' => 'dashboard'));
                }
                unset($data['Tweet']['content']);
            }
        }
    }

    public function register() {
        $this->layout = 'default';
        if ($this->request->is('post')) {
            $this->request->data['activation_key'] = $activation_key = Security::hash(Security::randomBytes(32));
            $data = $this->request->data;
            //pr($data); die;
            $this->User->set($data);

            $isValidated = $this->User->validates();
            $error = $this->User->validationErrors;
            $this->set(compact('error'));
            //if data is validated correctly
            if ($isValidated) {
                $this->User->save($data);
                $this->Session->setFlash("<div class='success-message flash notice'>User has been created successfully, Waiting for verification.</div>");
                $subject = "Account Activation link send on your email";
                $name = $this->request->data['name'];
                $to = trim($this->request->data['email']);
                //data ready for activation

                $Email = new CakeEmail();
                $Email->emailFormat('html');
                $Email->from(array('toperyah@gmail.com' => 'Microblog'));
                $Email->to($to);
                $Email->subject($subject);

                $activationUrl = $_SERVER['HTTP_HOST'] . "/users/activate/" . $activation_key  ;

                $message = "Dear <span style='color:#666666'>" . $name . "</span>,<br/><br/>";
                $message .= "Your account has been created successfully by Administrator.<br/>";
                $message .= "Please find the below details of your account: <br/><br/>";
                $message .= "<b>Full Name:</b> " . $this->data['name'] . "<br/>";
                $message .= "<b>Email Address:</b> " . $this->data['email'] . "<br/>";
                $message .= "<b>Username:</b> " . $this->data['username'] . "<br/>";
            
                $message .= "<b>Activate your account by clicking on the below url:</b> <br/>";
                $message .= "<a href='$activationUrl'>$activationUrl</a><br/><br/>";
                $message .= "<br/>Thanks, <br/>Support Team";
                $Email->send($message);


            } else {
                $this->Flash->error('There was a problem in registering the Account.');
            }
        }
    }

    public function activate($activation_key) {
        $this->autoRender = false;
        $user = $this->User->find('first', [
            'conditions' => [
                    ['User.activation_key' => $activation_key]
                ]
        ]);

        $id = $user['User']['id'];

        if (!$user) {
            throw new NotFoundException();
        }
        if (isset($user['User']['status']) && $user['User']['status'] !== '1' ) {
            $verify = $this->User->find('first',[
                'conditions' => [
                        ['User.activation_key' => $activation_key]
                    ]
                ]);
            if ($verify) {
                $this->User->updateAll(array('User.status' => '1'), array('User.id' => $id));
                $this->Flash->success(__('Account successfully verified!'));
                $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
            }
        } else {
            $this->Flash->error(__('Account was already verified!'));
        }
    }

    public function delete($id) {
        $this->autoRender = false;
        $this->loadModel('Tweet');

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        $delete = $this->Tweet->find('first', array(
            'conditions' => array(
                'Tweet.id' => $id
                )
            ));
            if (!empty($delete)){
                // if($delete['Tweet']['deleted'] == 0) {
                //     $deleteStatus = 1;
                // }
                $deleted = $this->Tweet->updateAll(array('Tweet.deleted' => 1), array('Tweet.id' => $id));
                if ($deleted) {
                    $this->Session->setFlash(__('Post Deleted Successfully'));
                } else {
                    $this->Session->setFlash(__('Something went wrong'));
                }
                $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
            }
        /*if ($this->Tweet->delete($id)) {
            $this->Flash->success(
            __('The post with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Flash->error(
            __('The post with id: %s could not be deleted.', h($id))
            );
        }*/
    }

    public function profile() {
        $this->layout = 'main';
        $this->loadModel('Tweet');

        //finds user infirmation
        $User = $this->User->find('first', [
            'conditions' => [
                'User.username' => $this->Session->read('username')
            ]
        ]);
        $this->set(compact('User'));

        $commentTweet = $this->Comment->find('all', array('joins' => array(
                array(
                    'table' => 'tweets',
                    'alias' => 'tweetJoin',
                    'type' => 'INNER',
                    'conditions' => array(
                        ['Comment.tweet_id = tweetJoin.id'],
                        //['Comment.deleted = 0']
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'userJoin',
                    'type' => 'INNER',
                    'conditions' => array(
                        ['Comment.user_id = userJoin.id'],
                        //['Comment.deleted = 0']
                    )
                )
            ),
        'order' => ['Comment.created' => 'desc']
        ));
        $this->set(compact('commentTweet'));

        $count = $this->Like->find('all', array(
            'conditions' => [
                'OR' => [
                    ['Like.is_liked' => 1]
                ]
            ]
        ));
        $this->set(compact('count'));

        $isLiked = $this->Like->find('all', array(
            'conditions' => [
                'AND' => [
                    'Like.is_liked' => 1
                ]
            ]
        ));
        $this->set(compact('isLiked'));

        $isFollowed = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.following_id' => $this->Auth->User('id'),
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowed'));

        $isFollowing = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.user_id' => $this->Auth->User('id'),
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowing'));

        $id = $User['User']['id'];

        //finds user specific tweets

        $this->Paginator->settings = [
            'Tweet' => [
                'limit' => 10,
                'order' => ['Tweet.created' => 'desc'],
                'conditions' => [
                    'AND' => [
                        ['Tweet.user_id' => $id],
                        ['Tweet.deleted' => 0]
                    ]
                ]
            ]
        ];
        $tweet = $this->Paginator->Paginate('Tweet');
        $this->set('tweet', $tweet);
    }

    public function edit($id = null) {
        $this->layout = 'main';

        $User = $this->User->find('first', [
            'conditions' => [
                'User.username' => $this->Session->read('username')
            ]
        ]);
        $this->set(compact('User'));

        $isFollowed = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.following_id' => $this->Auth->User('id'),
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowed'));

        $isFollowing = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.user_id' => $this->Auth->User('id'),
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowing'));
        
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Tweet->findById($id);
        $this->set('post', $post);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('edit', 'put'))) {
        $this->Tweet->id = $id;
            if ($this->Tweet->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'dashboard'));
            }
        $this->Flash->error(__('Unable to update your post.'));
        }
        if (!$this->request->data) {
                $this->request->data = $post;
        }
    }

    public function edit_user($id = null) {
        $this->layout = 'main';

        $User = $this->User->find('first', [
            'conditions' => [
                'User.username' => $this->Session->read('username')
            ]
        ]);
        $this->set(compact('User'));

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $find = $this->User->findById($id);
        if (!$find) {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('User', 'put'))) {
            $filename = '';

            if ($this->request->data['User']['image']['size'] > 0 ) {

                $uploadData = $this->request->data['User']['image']['tmp_name'];
                $filename = basename($this->request->data['User']['image']['name']);
                $uploadFolder = WWW_ROOT. 'img' ;
                $filename = md5($filename);
                $uploadPath =  $uploadFolder . DS . 'users' . DS .  $filename;


                if (!move_uploaded_file($uploadData, $uploadPath)) {
                    $this->Flash->error(__('Error Uploading Image, Please try again'));
                }
                $this->request->data['User']['image'] = $filename;
            } else {
                unset($this->request->data['User']['image']);
            }             

        $this->User->id = $id;
        unset($this->request->data['User']['password']);
        $data = $this->request->data;
        if ($this->User->save($data)) {
        $this->Flash->success(__('Your credentials has been updated.'));
        return $this->redirect(array('action' => 'profile'));
        }
        $this->Flash->error(__('Unable to update your credentials.'));
        }
        if (!$this->request->data) {
                $this->request->data = $find;
        }
    }

    public function search_result(){
        $this->layout = 'main';
        $this->loadModel('Tweet');
        $key = $this->params['url']['search'];

        $User = $this->User->find('first', [
            'conditions' => [
                'User.username' => $this->Session->read('username')
            ]
        ]);
        $this->set(compact('User'));

        $searchUser = $this->User->find('all', array(
            'conditions' => array(
                'OR' => [
                    'User.username LIKE' => "%$key%",
                    'User.name LIKE' => "%$key%"
                ]
            )
        ));
        $this->set('searchUser', $searchUser);


        $searchTweet = $this->Tweet->find('all', array(
            'conditions' => array(
                'OR' => [
                    'Tweet.content LIKE' => "%$key%",
                    'Tweet.retweet_content LIKE' => "%$key%",
                ] 
            )
        ));
        $this->set('searchTweet', $searchTweet);
    }

    public function other_user($id) {
        $this->layout = 'main';

        $User = $this->User->find('first', [
            'conditions' => [
                'User.username' => $this->Session->read('username')
            ]
        ]);
        $this->set(compact('User'));

        $Other = $this->User->find('first', [
            'conditions' => [
                'User.id' => $id
            ]
        ]);
        $this->set(compact('Other'));

        $count = $this->Like->find('all', array(
            'conditions' => [
                'AND' => [
                    ['Like.is_liked' => 1]
                ]
            ]
        ));
        $this->set(compact('count'));

        $isFollowed = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.user_id' => $id,
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowed'));


        $isFollowing = $this->Follower->find('count', array(
            'conditions' => [
                'AND' => [
                    'Follower.following_id' => $id,
                    'Follower.status' => 1
                ]
            ]
        ));
        $this->set(compact('isFollowing'));


        $this->Paginator->settings = [
            'Tweet' => [
                'limit' => 5,
                'order' => ['Tweet.created' => 'desc'],
                'conditions' => [
                    'AND' => [
                        ['Tweet.user_id' => $id],
                        ['Tweet.deleted' => 0]
                    ]
                ]
            ]
        ];
        $tweet = $this->Paginator->Paginate('Tweet');
        $this->set('tweet', $tweet);

        $isLiked = $this->Like->find('all', array(
            'conditions' => [
                'AND' => [
                    'Like.user_id' => $this->Auth->User('id'),
                    'Like.is_liked' => 1
                ]
            ]
        ));
        $this->set('isLiked', $isLiked);

        $commentTweet = $this->Comment->find('all', array('joins' => array(
                array(
                    'table' => 'tweets',
                    'alias' => 'tweetJoin',
                    'type' => 'INNER',
                    'conditions' => array(
                        ['Comment.tweet_id = tweetJoin.id'],
                        //['Comment.deleted = 0']
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'userJoin',
                    'type' => 'INNER',
                    'conditions' => array(
                        ['Comment.user_id = userJoin.id'],
                        //['Comment.deleted = 0']
                    )
                )
            ),
        'order' => ['Comment.created' => 'desc']
        ));
        $this->set(compact('commentTweet'));

        $try = $this->Follower->find('list', [
            'fields' => ['Follower.following_id'],
            'recursive' => -1,
            'group' => ['Follower.following_id'],
            'conditions' => [
                'Follower.user_id' => $this->Session->read('Auth.User.id')
            ]
        ]);
        
        $findFollowed = $this->Follower->find('first', array(
            'conditions' => [
                'AND' => [
                    ['Follower.following_id' => $id],
                    ['Follower.user_id' => $this->Auth->User('id')],
                    // ['Follower.status' => 1]
                ]
            ]
        ));
        $this->set('findFollowed', $findFollowed);

    }

    public function other_user_follower($id) {
        $this->layout = 'main';

        $followingUser = $this->Follower->find('all', [
            'conditions' => [
                'Follower.following_id' => $id,
                'Follower.status' => 1
            ]
        ]);

        $this->set(compact('followingUser'));


        $followUser = $this->Follower->find('all', array(
            'conditions' => [
                'AND' => [
                    'Follower.user_id' => $id,
                    'Follower.status' => 1
                ]
            ]
            ));
        
        $this->set(compact('followUser'));
    }

    
    public function followers() {
        $this->layout = 'main';

        $followingUser = $this->Follower->find('all', [
            'conditions' => [
                'Follower.following_id' => $this->Auth->User('id'),
                'Follower.status' => 1
            ]
        ]);

        $this->set(compact('followingUser'));

        //pr($followingUser); die;

        $followUser = $this->Follower->find('all', array(
            'conditions' => [
                'AND' => [
                    // 'Follower.following_id !=' => $this->Auth->User('id'),
                    'Follower.user_id' => $this->Auth->User('id'),
                    'Follower.status' => 1
                ]
            ]
            ));
        
        $this->set(compact('followUser'));

    }
}
