<div class="container emp-profile">
<?php $this->Flash->render()?>
    <form method="post">
        <div class="row">
            <div class="container-fluid well">
                <?php foreach ($searchUser as $su): ?>
                <div class="card border-light bg-white card proviewcard shadow-sm offset-1 col-md-9 offset-1">
						<div class="card-header"><h3>User</h3></div>
						<div class="card-body">
							<div class="col-lg-12 p-3 cardlist">
								<div class="col-lg-12">
									<div class="row">
										<div class="col-lg-8">
											<div class="row">
												<div class="col-4 col-lg-3 col-xl-2">
													<div class="row">
                                                    <?= $this->Html->image("/img/users/" . $su['User']['image'], ['class' => '', 'width' => '80', 'height' => '80']);?>
													</div>
												</div>
												<div class="col-8 col-lg-9 col-xl-10">
													<div class="d-block text-truncate mb-1">
                                                        @
                                                        <?php if ($User['User']['id'] == $su['User']['id']) {
                                                            $id = h($su['User']['id']);
                                                            echo $this->Html->Link($su['User']['username'], array(
                                                            'label' => false,
                                                            'role' => 'button',
                                                            'action' => 'profile'
                                                            )
                                                        );
                                                        ?>
                                                        <?php } else { 
                                                            $id = h($su['User']['id']);
                                                            echo $this->Html->Link($su['User']['username'], array(
                                                        'label' => false,
                                                        'role' => 'button',
                                                        'action' => 'other_user', $id
                                                            )
                                                        );
                                                        ?>
                                                        <?php } ?> 
													</div>
                                                    <div class="location d-block">
														<span><?php echo $su['User']['name']; ?> </span>
													</div>
													<div class="location d-block">
														<span><?php echo $su['User']['email']; ?> </span>
													</div>
													<div class="cartviewprice d-block">
                                                        <span>Location: <?php echo $su['User']['location']; ?> </span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-4 col-lg-3 col-xl-2 p-0 qty">
												</div>
											</div>
										</div>
										<div class="col-lg-3 ml-lg-auto align-self-start mt-2 mt-lg-0">

										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer border-light cart-panel-foo-fix">
						</div>
					</div>
                <?php endforeach; ?>
                <hr>
                <?php foreach ($searchTweet as $st): ?>
                <div class="card border-light bg-white card proviewcard shadow-sm">
						<div class="card-header"><h3>Tweet</h3></div>
                        <div class="card gedf-card offset-1 col-md-9 offset-1">   
                                <div class="card-header">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="mr-2">
                                            <?= $this->Html->image("/img/users/" . $su['User']['image'], ['class' => '', 'width' => '80', 'height' => '80']);?>
                                            </div>
                                            <div class="ml-2">
                                                <?php echo $su['User']['username'] ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        <?php echo $st['Tweet']['content']; ?>
                                    </p>
                                </div>
                            </div>
					</div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </form>           
</div>