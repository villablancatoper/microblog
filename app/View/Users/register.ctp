<div class="card bg-light">
<?php echo $this->Flash->render(); ?>
<article class="card-body mx-auto" style="width: 400px;">
	<h4 class="card-title mt-3 text-center">Create Account</h4>
	<p class="text-center">Get started with your free account</p>
	<form method="post">
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-at"></i> </span>
			</div>
			<?php echo $this->Form->input('username', array('label' => false, 'size' => '35', 'class' => "form-control", 'placeholder' => 'Username')); ?>
			<span> Please enter 5 characters or more </span>
			<div class="error-message"><?= $error['username'][0] ?? null; ?></div>
		</div> <!-- form-group// -->
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
			</div>
			<?php echo $this->Form->input('password', array('label' => false, 'size' => '35', 'class' => "form-control", 'placeholder' => 'Password', 'type' => 'password')); ?>
			<span> Please enter 5 characters or more </span>
			<div class="error-message"><?= $error['password'][0] ?? null; ?></div>
		</div> <!-- form-group// -->
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
			</div>
			<div>
			<?php echo $this->Form->input('email', array('label' => false, 'size' => '35', 'class' => "form-control", 'placeholder' => 'Email Address', 'type' => 'email')); ?>
			</div>
			<div class="error-message"><?= $error['email'][0] ?? null; ?></div>
		</div> <!-- form-group// -->
		<div class="form-group input-group">
			<div class="input-group-prepend">
				<span class="input-group-text"> <i class="fa fa-user"></i> </span>
			</div>
			<?php echo $this->Form->input('name', array('label' => false, 'size' => '35', 'class' => "form-control", 'placeholder' => 'Full name')); ?>
			<div class="error-message"><?= $error['name'][0] ?? null; ?></div>
		</div> <!-- form-group// -->
                                
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
		</div> <!-- form-group// -->
		<p class="text-center">Have an account? <a href="<?php echo $url ?>">Log In</a> </p>                                                                 
	</form>
</article>
</div> <!-- card.// -->

</div> 
<!--container end.//-->

<br><br>
<article class="bg-secondary mb-3">  
<div class="card-body text-center">
    <h3 class="text-white mt-3">Bootstrap 4 UI KIT</h3>
<p class="h5 text-white">Components and templates  <br> for Ecommerce, marketplace, booking websites 
and product landing pages</p>   <br>
<p><a class="btn btn-warning" target="_blank" href="http://bootstrap-ecommerce.com/"> Bootstrap-ecommerce.com  
 <i class="fa fa-window-restore "></i></a></p>
</div>
<br><br>
</article>