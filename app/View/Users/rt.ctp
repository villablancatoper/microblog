<div class="main-section-data">
  <div class="row">
    <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
      <div class="main-ws-sec">
        <div class="posts-section">
          <?php foreach ($posts as $post): ?>
          <div class="post-bar">
            <div class="post_topbar">
              <div class="usy-dt">
              <?php foreach ($user as $users): ?>
                <?= $this->Html->image("profile/" . h($users['User']['image']));?>
                <div class="usy-name">
                  <h3>
                  <?php
                    $userId = HashidsComponent::encode(AuthComponent::user('id'));
                    echo $this->Html->link(h($users['User']['first_name']). " " . h($users['User']['last_name']),
                      ['controller' => 'users',
                      'action' => 'view',
                      $userId]) ?>
                  </h3>
                  <span> <?= h($users['User']['username']) ?> </span>
                </div>
                <!--/ usy-name -->
                <?php endforeach; ?>
              </div>
              <!-- /usy-dt -->
            </div>
            <!--/ post_topbar -->
            <div class="post_content">
              <?= $this->Form->create('Post', array('enctype' => 'multipart/form-data'));?>
              <div class="form-group">
                <label for="content">
                <?= $this->Form->textarea('retweet_content',
                  ['rows' => '2',
                  'cols' => '100',
                  'maxlength' => 140,
                  'class' => 'form-control',
                  'placeholder' => 'Add caption or comment...']);?>
                  <small class="form-text text-muted" style="float:left;" >
                    <span id="remainingCharacter">140</span>
                      Character(s) Remaining
                  </small><br>
              </div>
              <!--/ form-group -->
              <?php if (isset($post['Post']['post_id'])) {?>
              <div class="retweet-bar col-md-11 offset-md-1">
                <div class="retweet_bar">
                  <div class="post_topbar">
                    <div class="usy-dt">
                      <?= $this->Html->image("profile/" . h($post['AuthorRetweet']['image']));?>
                      <div class="usy-name">
                        <h6>
                        <?php
                          $authorId = HashidsComponent::encode($post['AuthorRetweet']['id']);
                          echo $this->Html->link(h($post['AuthorRetweet']['first_name']). " " . h($post['AuthorRetweet']['last_name']),
                            ['controller' => 'users',
                            'action' => 'view',
                            $authorId]);?>
                        </h6>
                        <span>
                          <i class="fa fa-clock-o"></i>
                          <?= $this->Time->niceShort(h($post['Post']['created'])) ?>
                        </span>
                      </div>
                      <!--/ usy-name -->
                    </div>
                    <!--/ usy-dt -->
                  </div>
                  <!--/ post_topbar -->
                  <div class="post_content">
                    <?php
                      if (empty($post['Post']['image'])) {
                        echo '<p>';
                          echo h($post['Post']['retweet_content']);
                        echo '</p>';
                      } else {
                        echo '<p>';
                          echo h($post['Post']['retweet_content']);
                        echo '</p>';
                        echo $this->Html->image("posts/" . h($post['Post']['image']));
                      } ?>
                  </div>
                  <!--/ post_content -->
                </div>
                <!--/ retweet_bar -->
              </div>
              <!--/ retweet-bar col-md-11 offset-md-1 -->
              <?php } else { ?>
              <div class="retweet-bar col-md-11 offset-md-1">
                <div class="retweet_bar">
                  <div class="post_topbar">
                    <div class="usy-dt">
                      <?= $this->Html->image("profile/" . h($post['User']['image']));?>
                      <div class="usy-name">
                        <h6>
                          <?php
                            $postAuthorId = HashidsComponent::encode($post['User']['id']);
                            echo $this->Html->link(h($post['User']['first_name']). " " . h($post['User']['last_name']),
                              ['controller' => 'users',
                              'action' => 'view',
                              $postAuthorId]);
                          ?>
                        </h6>
                        <span>
                          <i class="fa fa-clock-o"></i>
                          <?= $this->Time->niceShort(h($post['Post']['created'])) ?>
                        </span>
                      </div>
                      <!--/ uusy-name -->
                    </div>
                    <!--/ usy-dt -->
                  </div>
                  <!--/ post_topbar -->
                  <div class="post_content">
                  <?php
                    if (empty($post['Post']['image'])) {
                      echo '<p>';
                        echo h($post['Post']['content']);
                      echo '</p>';
                    } else {
                      echo '<p>';
                        echo h($post['Post']['content']);
                      echo '</p>';
                      echo $this->Html->image("posts/" . h($post['Post']['image']));
                    }?>
                  </div>
                </div>
              </div>
              <?php } ?>
              <div class="form-group">
                <?php
                  echo $this->Form->button('Retweet Post',
                    ['action' => 'retweet',
                    'class' => 'btn btn-primary float-right',
                    'onclick' => "this.form.submit(); this.disabled=true;"]);
                  echo $this->Form->end();
                ?><br>
              </div>
              <!--/ form-group -->
            </div>
            <!--/ post_content -->
          </div>
          <!--/ post-bar -->
          <?php endforeach; ?>
        </div>
        <!--/ main-section -->
      </div>
      <!--/ main-ws-sec -->
    </div>
    <!--/ col-lg-6 col-md-8 no-pd -->
  </div>
  <!--/ row -->
</div>
<!--/ main-section-data -->
