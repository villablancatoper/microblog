<div class="container">
<?php echo $this->Flash->render(); ?>
	<div class="d-flex justify-content-center h-100">
		<div class="card" style="margin: 100px">
			<div class="card-header" style="background-color: rgb(239, 134, 74)">
				<h3>MICROBLOG</h3>
			</div>
			<div class="card-body" style="width: 420px">
				<div class="users form">
					<?php echo $this->Form->create('User'); ?>
						<fieldset>
							<?php echo $this->Form->input('username', array(
								'label' => false,
								'value' => '',
								'type' => 'text',
								'class' => 'form-control',
								'placeholder' => 'User ID',
								'style' => 'margin-top: 10px'));
								
								echo $this->Form->input('password', array(
								'label' => false,
								'value' => '',
								'type' => 'password',
								'class' => 'form-control',
								'placeholder' => 'Password',
								'style' => 'margin-top: 50px'));
							?>
							<div id="inputButton">
                           <?= $this->Form->input('Login',
                          ['type' => 'button',
							'label' => false,
							'id' => 'login',
							'class' => 'btn btn-primary float-right',
							'style' => 'margin-top: 20px'
                          ]);?>
        	                <?= $this->Form->end() ?>
						</fieldset>
                       </div>						
				</div>
			<div class="card-footer" style="background-color: rgb(239, 134, 74)">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="<?php echo $url ?>register">Sign Up</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Forgot your password?</a>
				</div>
			</div>
		</div>
	</div>
</div>