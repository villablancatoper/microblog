<div class="container">
    <?php echo $this->Flash->render()?>
    <div class="container-fluid gedf-wrapper">
        <div class="row">
                <!-- User Details -->
                    <div class="col-md-3">
                        <div class="card-new" style="background-color: #30d7e3">
                            <div class="profile-userpic">
                                <?= $this->Html->image("/img/users/" . h($User['User']['image']), ['class' => 'img-responsive', 'style' => 'margin-left: 55px']);?>
                            </div>
                            <div class="card-body">
                                <div class="h5">@<?php echo $User['User']['username']; ?></div>
                                <div class="h7 text-muted">Fullname : <?php echo h($User['User']['name']); ?></div>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="h6 text-muted">Followers</div>
                                    <div class="h5"><?php echo $isFollowed; ?></div>
                                </li>
                                <li class="list-group-item">
                                    <div class="h6 text-muted">Following</div>
                                    <div class="h5"><?php echo $isFollowing; ?></div>
                                </li>
                                <li class="list-group-item active" style="background-color: #80e4e3db">
                                    <a href="<?php echo $url ?>users/dashboard">
                                        <i class="fa fa-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="<?php echo $url ?>users/profile">
                                        <i class="fa fa-user"></i>
                                        Profile 
                                    </a>
                                </li>
                                <!-- <li class="list-group-item">
                                    <a href="#">
                                        <i class="fa fa-bell"></i>
                                        Notifications
                                    </a>
                                </li> -->
                                <li class="list-group-item">
                                    <a href="<?php echo $url ?>users/followers">
                                        <i class="fa fa-users"></i>
                                        Followers 
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                <!-- User Details End -->
                <div class="col-md-7 gedf-main">
                <!-- Main Tweet Section-->
                    <div class="card gedf-card">
                        <div class="card-header" style="background-color: rgb(245, 174, 174)">
                            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                                        a publication</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#images">Images</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                    <div class="form-group">
                                        <label class="sr-only" for="message">post</label>
                                        <?php echo $this->Form->create('Tweet', [
                                            'id' => 'saveTweet', 
                                            'class' => 'saveTweet'
                                            ]);
                                            echo $this->Form->input('user_id',
                                            ['id' => 'user_id',
                                            'value' => $User['User']['id'],
                                            'type' => 'hidden',
                                            'label' => false]);
                                            echo $this->Form->input('content', array(
                                            'label' => false,
                                            'type' => 'text',
                                            'text' => '',
                                            'rows' => '4',
                                            'maxlength' => '140',
                                            'class' => 'form-control',
                                            'placeholder' => 'Please enter up to 140 characters',
                                            'style' => 'margin-top: 10px'));
                                        ?>
                                    </div>
                                    <div id="inputButton">
                                                <?= $this->Form->input('Submit',
                                                    ['type' => 'button',
                                                    'label' => false,
                                                    'id' => 'submitPost',
                                                    'class'=>'btn btn-primary float-right',
                                                    ]);?>
                                                <?= $this->Form->end() ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                                    <div class="form-group">
                                        <div class="custom-file">
                                            <?php echo $this->Form->create('Tweet', [
                                                'id' => 'postPicture',
                                                'enctype' => 'multipart/form-data' 
                                                ]);
                                            ?>
                                        <div>            
                                            <?php
                                                echo $this->Form->input('user_id',
                                                ['id' => 'user_id',
                                                'value' => $User['User']['id'],
                                                'type' => 'hidden',
                                                'label' => false]);
                                                echo $this->Form->input('picture', array(
                                                    'label' => false,
                                                    //'class' => 'custom-file-label',
                                                    'type' => 'file',
                                                ));
                                            ?>
                                            
                                        </div>
                                        </div>
                                        <div id="inputButton">
                                                    <?= $this->Form->input('Submit',
                                                        ['type' => 'button',
                                                        'label' => false,
                                                        'id' => 'submitPost',
                                                        'class'=>'btn btn-primary float-right',
                                                        'style' => 'margin-top: 10px',
                                                        ]);?>
                                                    <?= $this->Form->end() ?>
                                            </div>
                                        </div>
                                    <div class="py-4">
                                    <img id="blah" width="250" height="180"/>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="btn-toolbar justify-content-between">
                                <!-- <div class="btn-group">
                                    <button type="submit" class="btn btn-primary float-right">Share</button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                <!-- Main Tweet Section End-->                                          
                <!-- Replies and Other Functions-->
                    <?php foreach ($userTweet as $t): ?>
                        <?php ?>
                            <br />                 
                                <!-- if card is retweeted -->
                                <?php if (!empty($t['Tweet']['retweet_id'])) { ?>
                                    <div class="card gedf-card"> 
                                        <div class="card-header" style="background-color: rgb(245, 174, 174)">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="mr-2">
                                                        <?= $this->Html->image("/img/users/" . h($t['Retweeter']['image']), ['class' => 'rounded-circle', 'width' => '45']);?>
                                                    </div>
                                                    <div class="ml-2">
                                                        <div class="h5 m-0"><?php echo h($t['Retweeter']['username']); ?> retweeted: </div>
                                                    </div>
                                                </div>
                                                <div>
                                                <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                    <div class="dropdown">
                                                        <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-ellipsis-h"></i>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                        <?php echo $this->Form->postLink(
                                                            'Delete',
                                                            array('class' => 'dropdown-item', 'action' => 'delete', $t['Tweet']['id']),
                                                            array('confirm' => 'Are you sure?')
                                                            );
                                                        ?>
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div>
                                                <p class="card-text">
                                                    <?php echo h($t['Tweet']['retweet_content']); ?>
                                                </p>
                                            </div>
                                            <hr>
                                                <div class="card gedf-card offset-1">   
                                                    <div class="card-header" style="background-color: rgb(245, 174, 174)">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <div class="mr-2">
                                                                    <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                </div>
                                                                <div class="ml-2">
                                                                    <div class="h5 m-0"><?php echo h($t['User']['username']); ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div>
                                                        <?= $this->Html->image("/img/users/" . $t['Tweet']['retweet_picture'], ['class' => 'box', 'width' => '300', 'height' => '300']);?>
                                                        </div>
                                                        <p class="card-text">
                                                            <?php echo h($t['Tweet']['content']); ?>
                                                        </p>
                                                </div>
                                        </div>
                                        <div class="card-footer">
                                            <!-- Tweet Utilites -->
                                                    <p> Created: <?php echo $this->Time->niceshort($t['Tweet']['created']); ?> </p>
                                                    <?php $ctr = 0 ?>
                                                    <?php foreach ($isLiked as $il):?>
                                                        <?php if ($il['Like']['tweet_id'] == $t['Tweet']['id']) {
                                                            
                                                        }
                                                        ?>
                                                    <?php endforeach ?>
                                                    <?php foreach ($count as $c):?>
                                                        <?php if ($c['Like']['tweet_id'] == $t['Tweet']['id']) {
                                                            $ctr++;
                                                        }
                                                        ?>
                                                    <?php endforeach ?>
                                                    <span class="fa fa-thumbs-up" id="likeCount<?php echo $t['Tweet']['id']?>"><?php echo $ctr; ?></span> 
                                                    <br />
                                                    <hr>
                                                    <?php if (!empty($t['Comment'])) :?>
                                                        <a href="#a" id="more<?php echo $t['Tweet']['id']?>">View Replies</a>
                                                    <?php endif; ?>
                                                    <div id="replies<?php echo $t['Tweet']['id']?>" class="form-group-replies" style="display: none">
                                                    <!-- Comment Section -->
                                                    <?php foreach ($commentTweet as $c): ?>
                                                        <div id="#a reply-header<?php echo $t['Tweet']['id']?>">
                                                            <div class="card gedf-card">
                                                                <?php if ($t['Tweet']['id'] == $c['Tweet']['id']) { ?>
                                                                    <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                                        <div class="d-flex justify-content-between align-items-center">
                                                                            <div class="d-flex justify-content-between align-items-center">
                                                                                <div class="mr-2">
                                                                                <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                </div>
                                                                                <div class="ml-2">
                                                                                    <div class="h5 m-0"><?php echo h($c['User']['username']); ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                                    
                                                                    <div class="card-body">
                                                                        <p class="card-text">
                                                                            <?php echo h($c['Comment']['comment']); ?>
                                                                        </p>
                                                                    </div>
                                                                    <hr>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                    <!-- COmment Section end -->
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <?php if (isset($il['Like']['tweet_id'] ) && $il['Like']['tweet_id'] == $t['Tweet']['id']) :?>
                                                        <button id="liked_post-<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary likeChange" data-id="<?php echo $t['Tweet']['id']?>" data-user="<?php echo $User['User']['id'] ?>">
                                                            <span class="fa fa-thumbs-o-up fa-thumbs-up"></span> Liked
                                                        </button>
                                                    <?php else :?>
                                                        <button id="like_post-<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary likeChange" data-id="<?php echo $t['Tweet']['id']?>" data-user="<?php echo $User['User']['id'] ?>">
                                                            <span class="fa fa-thumbs-o-up"></span> Like
                                                        </button>
                                                    <?php endif; ?>
                                                    <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                    <span class="fa fa-comment"></span> Comment
                                                    </button>
                                                    <button id="formRetweet<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                    <span class="fa fa-retweet"></span> Retweet
                                                    </button>
                                                    <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                        <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-edit"></span>
                                                        <?php echo $this->Html->Link('Edit', array(
                                                            'label' => false,
                                                            'class' => 'btn btn-outline-secondary',
                                                            'role' => 'button',
                                                            'action' => 'edit', $t['Tweet']['id']
                                                                )
                                                            );
                                                        ?>
                                                    <?php } else { ?>
                                                    <?php } ?>
                                                    </button>
                                                    <div id="reply-body<?php echo $t['Tweet']['id']?>" class="form-group-reply" style="display: none">
                                                            <label class="sr-only" for="message">post</label>
                                                            <?php echo $this->Form->create('Comment', [
                                                                'id' => 'reply'. $t['Tweet']['id']
                                                                ]);
                                                                echo $this->Form->input('comment', array(
                                                                'label' => false,
                                                                'type' => 'text',
                                                                'text' => '',
                                                                'rows' => '3',
                                                                'maxlength' => '140',
                                                                'class' => 'form-control',
                                                                'placeholder' => 'Please enter up to 140 characters',
                                                                'style' => 'margin-top: 10px'));
                                                                echo $this->Form->input('user_id',
                                                                ['id' => 'user_id',
                                                                'value' => $User['User']['id'],
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                                echo $this->Form->input('tweet_id',
                                                                ['id' => 'tweet_id',
                                                                'value' => $t['Tweet']['id'],
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                            ?>
                                                            <div id="inputButton">
                                                                <?= $this->Form->input('Reply',
                                                                ['type' => 'button',
                                                                'label' => false,
                                                                'id' => 'submitReply',
                                                                'class'=>'btn btn-primary float-right'
                                                                ]);?>
                                                            <?= $this->Form->end() ?>
                                                        </div>
                                                    </div>
                                                    
                                            <!-- Tweet Utilities End -->
                                        </div>                                
                                        </div>
                                    </div>
                                <!-- if tweet is original -->                                            
                                <?php } else { ?>
                                    <div class="card gedf-card">   
                                        <div class="card-header" style="background-color: rgb(245, 174, 174)">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="mr-2">
                                                    <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                    </div>
                                                    <div class="ml-2">
                                                        <div class="h5 m-0"><?php echo $t['User']['username']; ?></div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                        <div class="dropdown">
                                                            <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-ellipsis-h"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                                                <?php echo $this->Form->postLink(
                                                                    'Delete',
                                                                    array('class' => 'dropdown-item', 'action' => 'delete', $t['Tweet']['id']),
                                                                    array('confirm' => 'Are you sure?')
                                                                    );
                                                            ?>
                                                            </div>
                                                        </div>
                                                    <?php } else { ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div>
                                            <?= $this->Html->image("/img/users/" . $t['Tweet']['picture'], ['class' => '', 'width' => '300', 'height' => '300']);?>
                                            </div>
                                            <p class="card-text">
                                                <?php echo h($t['Tweet']['content']); ?>
                                            </p>
                                        </div>                                        
                                        <div class="card-footer">
                                            <!-- Tweet Utilites -->
                                                    <p> Created: <?php echo $this->Time->niceshort($t['Tweet']['created']); ?> </p>
                                                    <?php $ctr = 0 ?>
                                                    <?php foreach ($isLiked as $il):?>
                                                        <?php if ($il['Like']['tweet_id'] == $t['Tweet']['id']) {
                                                            $ctr++;
                                                        }
                                                        ?>
                                                    <?php endforeach ?>
                                                    <span class="fa fa-thumbs-up" id="likeCount<?php echo $t['Tweet']['id']?>"><?php echo $ctr?></span>
                                                    <br />
                                                    <hr>
                                                    <?php if (!empty($t['Comment'])) :?>
                                                        <a href="#a" id="more<?php echo $t['Tweet']['id']?>">View Replies</a>
                                                    <?php endif; ?>
                                                    <div id="replies<?php echo $t['Tweet']['id']?>" class="form-group-replies" style="display: none">
                                                    <?php foreach ($commentTweet as $c): ?>
                                                        <div id="#a reply-header<?php echo $t['Tweet']['id']?>">
                                                            <div class="card gedf-card">
                                                                <?php if ($t['Tweet']['id'] == $c['Tweet']['id']) { ?>
                                                                    <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                                        <div class="d-flex justify-content-between align-items-center">
                                                                            <div class="d-flex justify-content-between align-items-center">
                                                                                <div class="mr-2">
                                                                                <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                                                </div>
                                                                                <div class="ml-2">
                                                                                    <div class="h5 m-0"><?php echo h($c['User']['username']); ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                                    
                                                                    <div class="card-body">
                                                                        <p class="card-text">
                                                                            <?php echo h($c['Comment']['comment']); ?>
                                                                        </p>
                                                                    </div>
                                                                    <hr>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <?php if (isset($il['Like']['tweet_id'] ) && $il['Like']['tweet_id'] == $t['Tweet']['id']) :?>
                                                        <button id="liked_post-<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary likeChange" data-id="<?php echo $t['Tweet']['id']?>" data-user="<?php echo $User['User']['id'] ?>">
                                                            <span class="fa fa-thumbs-o-up fa-thumbs-up"></span> Liked
                                                        </button>
                                                    <?php else :?>
                                                        <button id="like_post-<?php echo $t['Tweet']['id']?>" class="btn btn-outline-secondary likeChange" data-id="<?php echo $t['Tweet']['id']?>" data-user="<?php echo $User['User']['id'] ?>">
                                                            <span class="fa fa-thumbs-o-up"></span> Like
                                                        </button>
                                                    <?php endif; ?>
                                                    <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-comment"></span> Comment 
                                                    </button>
                                                    <button id="formRetweet<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-retweet"></span> Retweet
                                                    </button>
                                                    <?php if ($t['User']['username'] == $User['User']['username']) { ?>
                                                        <button id="formButton<?php echo $t['Tweet']['id']?>" name="" class="btn btn-outline-secondary comment">
                                                        <span class="fa fa-edit"></span>
                                                        <?php echo $this->Html->Link('Edit', array(
                                                            'label' => false,
                                                            'class' => 'btn btn-outline-secondary',
                                                            'role' => 'button',
                                                            'action' => 'edit', $t['Tweet']['id']
                                                                )
                                                            );
                                                        ?>
                                                    <?php } else { ?>
                                                    <?php } ?>
                                                    </button>
                                                    <div id="reply-body<?php echo $t['Tweet']['id']?>" class="form-group-reply" style="display: none">
                                                            <label class="sr-only" for="message">post</label>
                                                            <?php echo $this->Form->create('Comment', [
                                                                'id' => 'reply'. $t['Tweet']['id']
                                                                ]);
                                                                echo $this->Form->input('comment', array(
                                                                'label' => false,
                                                                'type' => 'text',
                                                                'text' => '',
                                                                'rows' => '3',
                                                                'maxlength' => '140',
                                                                'class' => 'form-control',
                                                                'placeholder' => 'Please enter up to 140 characters',
                                                                'style' => 'margin-top: 10px'));
                                                                echo $this->Form->input('user_id',
                                                                ['id' => 'user_id',
                                                                'value' => $User['User']['id'],
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                                echo $this->Form->input('tweet_id',
                                                                ['id' => 'tweet_id',
                                                                'value' => $t['Tweet']['id'],
                                                                'type' => 'hidden',
                                                                'label' => false]);
                                                            ?>
                                                            <div id="inputButton">
                                                                <?= $this->Form->input('Reply',
                                                                ['type' => 'button',
                                                                'label' => false,
                                                                'id' => 'submitReply',
                                                                'class'=>'btn btn-primary float-right'
                                                                ]);?>
                                                            <?= $this->Form->end() ?>
                                                        </div>
                                                    </div>
                                                    
                                            <!-- Tweet Utilities End -->
                                        </div>
                                    </div>
                                <?php } ?>
                                <!-- Modal -->
                                    <div class="modal fade" id="retweet<?php echo $t['Tweet']['id']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Preview</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                        <div id="retweet-body<?php echo $t['Tweet']['id']?>" class="form-group-retweet">
                                                <label class="sr-only" for="message">post</label>
                                                <?php echo $this->Form->create('Tweet', [
                                                    'id' => 'retweet'. $t['Tweet']['id']
                                                    ]);
                                                    echo $this->Form->input('retweet_content', array(
                                                    'label' => false,
                                                    'type' => 'text',
                                                    'value' => '',
                                                    'rows' => '3',
                                                    'maxlength' => '140',
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Please enter up to 140 characters',
                                                    'style' => 'margin-top: 10px'));
                                                    echo $this->Form->input('retweet_user_id',
                                                    ['id' => 'user_id',
                                                    'value' => $User['User']['id'],
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                    echo $this->Form->input('content',
                                                    ['id' => 'content',
                                                    'value' => h($t['Tweet']['content']),
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                    echo $this->Form->input('retweet_id',
                                                    ['id' => 'tweet_id',
                                                    'value' => $t['Tweet']['id'],
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                    echo $this->Form->input('retweet_picture',
                                                    ['id' => 'picture',
                                                    'value' => $t['Tweet']['picture'],
                                                    'type' => 'hidden',   
                                                    'label' => false]);
                                                    echo $this->Form->input('user_id',
                                                    ['id' => 'user_id',
                                                    'value' => $User['User']['id'],
                                                    'type' => 'hidden',
                                                    'label' => false]);
                                                ?>
                                        </div>
                                        </div>
                                        <div class="card gedf-card">    
                                            <div class="card-header" style="background-color: rgb(206, 206, 206)">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="mr-2">
                                                            <?= $this->Html->image("/img/users/" . $t['User']['image'], ['class' => 'rounded-circle', 'width' => '45']);?>
                                                        </div>
                                                        <div class="ml-2">
                                                            <div class="h5 m-0"><?php echo h($t['User']['username']); ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                            <div>
                                            <?= $this->Html->image("/img/users/" . $t['Tweet']['picture'], ['class' => 'box', 'width' => '300', 'height' => '300']);?>
                                            </div>
                                                <p class="card-text">
                                                    <?php echo h($t['Tweet']['content']); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <div id="inputButton">
                                                    <?= $this->Form->input('Retweet',
                                                    ['type' => 'button',
                                                    'label' => false,
                                                    'id' => 'submitRetweet',
                                                    'class'=>'btn btn-primary float-right'
                                                    ]);?>
                                                <?= $this->Form->end() ?>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                <!-- Modal End -->
                    <?php endforeach; ?>
                <!-- Replies and Other Functions End-->
                </div>                
        </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    <?php foreach ($userTweet as $t): ?>
        $('#formButton<?php echo $t['Tweet']['id']?>').on('click', function() {
            $('#like_post-<?php echo $t['Tweet']['id']?>').each(function () {           
                $('#reply-body<?php echo $t['Tweet']['id']?>').slideToggle();
            });
        });
        $('#formButton<?php echo $t['Tweet']['id']?>').on('click', function() {
            $('#liked_post-<?php echo $t['Tweet']['id']?>').each(function () {           
                $('#reply-body<?php echo $t['Tweet']['id']?>').slideToggle();
            });
        });
        $('#more<?php echo $t['Tweet']['id']?>').on('click', function(event) {
            event.preventDefault();
            $('#more<?php echo $t['Tweet']['id']?>').each(function () {            
                $('#replies<?php echo $t['Tweet']['id']?>').slideToggle('fast');
            });
        });
        $('#formRetweet<?php echo $t['Tweet']['id']?>').on('click', function(event) {
            event.preventDefault();
            $('#like_post-<?php echo $t['Tweet']['id']?>').each(function () {            
                $('#retweet<?php echo $t['Tweet']['id']?>').modal('show');
            });
        });
        $('#formRetweet<?php echo $t['Tweet']['id']?>').on('click', function(event) {
            event.preventDefault();
            $('#liked_post-<?php echo $t['Tweet']['id']?>').each(function () {            
                $('#retweet<?php echo $t['Tweet']['id']?>').modal('show');
            });
        });
        <?php endforeach; ?>

        $('body').delegate('.likeChange','click', function() {
            let id = $(this).data('id');
            let user_id = $(this).data('user');
            
            if ($.trim($('#like_post-'+id).text()) == 'Like' || $.trim($('#liked_post-'+id).text()) == 'Like') {
                $.ajax({
                    type: 'POST',
                    url: "like",
                    dataType: 'json',
                    data: {
                        user_id: user_id, 
                        tweet_id: id
                    },
                    success: function (response) {
                        console.log(response);
                        $('#likeCount'+id).text(response['data_count']);
                        $('#like_post-'+id).text('Liked');
                        $('#liked_post-'+id).text('Liked');
                        console.log($('#liked_post-'+id).find('span'));
                        $('#liked_post-'+id).find('span').removeClass('fa fa-thumbs-o-up');
                        $('#liked_post-'+id).find('span').addClass('fa fa-thumbs-o-up fa-thumbs-up');
                    }
                });
            } else {
                let id = $(this).data('id');
                let user_id = $(this).data('user');
                $.ajax({
                    type: 'POST',
                    url: "unlike",
                    dataType: 'json',
                    data: {
                        user_id: user_id, 
                        tweet_id: id
                    },
                    success: function(response){
                        $('#liked_post-'+id).text('Like');
                        $('#like_post-'+id).text('Like');
                        $('#likeCount'+id).text(response['data_count']);
                        $('#liked_post-'+id).find('span').removeClass('fa fa-thumbs-o-up fa-thumbs-up');
                        $('#liked_post-'+id).find('span').addClass('fa fa-thumbs-o-up');
                    }
                });
            }
        });

        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
            }
        }

        $("#TweetPicture").change(function() {
            readURL(this);
        });

    });
</script>