<div class="container emp-profile">
<?php echo $this->Flash->render()?>
    <h1>Edit Profile</h1>
  	<hr>
	<div class="row">
      <!-- left column -->
      <div class="col-md-2">
      </div>
      
      <!-- edit form column -->
      <div class="col-md-10 personal-info">
        <h3>Personal info</h3>

        <?php
            echo $this->Form->create('User', array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'enctype' => 'multipart/form-data'
            ));
        ?>
            <div class="form-group">
                <label class="col-lg-3 control-label">Profile Picture:</label>
                    <div class="col-lg-8">
                    <div class="text-center">
                    <img id="blah" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEX///9ZWVlYWVv8/PxVVVVdXV35+flra2vg4OBaWlpMTExISEhSUlL39/d5eXlVVlhzc3Pa2tqOjo6CgoLm5ubt7e1OT1GKiorr6+uxsbGVlZWenp7S0tK2tranp6dRUlXIyMhlZWW/wMKampp0dXhnaGtCQkJv0E8FAAAHrUlEQVR4nO2bi3KbOhCGBZKwJQQCc7HxDZs0ff9HPLuS7Tg2OGfatMB0v8lkUpAovyXtTTJjBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBPFvwhnnr27j/ZctJk/EWfRFk4R/1WLScFZ12/V6veoFbxzzeQ9ispNaGyMGMVqny7Hf8pfhvMq0UMFLlArkEmfzHOG80F/oQ0Rwrr5crRPlpJUyX+hTSgl9nKvCQqAGWIZmGCGh0bma6CzlYOiHLSGv3sMgkOZtnw/T4YcQ6iX/9FjOEz4JJ4IO+8XtyoaBCs7YcKhJxLYg0dannmdPYVg5S067AV+H7i4MYZEtGI+ivgEBDVHEGliooU0feq6acgoCGW/OsJCCPj9nbWttGIKhXLjYjXXFA2+Vi3caMLdhaB0fXhIeu0vYi8H/C8AKjLaDhjK8IKTJXPN9/Oj2deHmoRvDC/cmNtDZZlwLy3GCiWDI313fWQRe4dY8thSqxBuHAYUqMGsWjSkxiUoNlvKLMQyUblzzRovHNmechiyPexVCa6nzEfWhnWj8HI0htBwmXvvmVfp4x5z8ncb8fOplNH50ejfqOuRsjaNi0v3yFZV/Sfi1ebiT+DsRS8qHO/lyedQ4jotR7SlnGSrU1UufeM2AexpdnWmvZ4/c9P85qk/kPEWFccISF4QMEPmsAf7g12z+7i67pfmP4Men4nFTjpvC12/B/UD6GVltqmqTuEFD7dcJ/NzponBkr///FGIlw2lZNsUiQDOistWh7Ju3dziFwVwUukC6S83NX0DMEhT76JU/n5lCSBQXzndKFyAo/CXACL/qMx+FEHtGm6LGWobw6iT8KaUNbYuR50Dn+SiE7IPlYRteYk0FYTpm9S6CsW1WDbmDGSnk7FRDkoH6hIlFmqaZjo1FgfDjQ9O+fvNRyPa1jzhFnHWVv1g27zFck6GVshroNyWF0dNM4744iP6Ab6wbLWWKJfPO0VnQ0zsKt8pkyTUOiG4fFXSLpqSwx+jzW6gWpS0qseLILpJddAPB6FuLCbLUb9edjc9li+koNPu+OpPLi/CVD1q6JbeHbItjUFNWOC/x7wNIVNJAiuQ+oigp75+wCKaiMNDmKX0y8eoScW7OoE6Fds9wLpe7TGp9LpqNV29CC+szjRJsnRTxtX+ta2Omo/AZJVTsvDlnR3xRZQ6ottrVtbOhbSwbjMPZDmyqDeuTW4BdLK7PuyXEU1UIfs9nt7w6u4m8Zjzh1aL2742qTIF7askC/IisC/e0xsjb86alEAtjn3eT4ILGxQaeAvPYwJS4R7PwRtVLFKbAAuPJibZu72l59xzb2ukoNNunMul2tb04cqzFKbOFwI2vW+tGz4+hK9/APP2B/6w71zh/u3+InIxC3Z8guFdLMizG6T2OZhveE0ihwNywRofStutbjw9+2KkojJPe1N412AisNhocrJW19wrhujnC9dIN6vvN1X9k/pNS+HTjeiV3y7DArW75aQhDiV4CvSL+baVftR8FGwiKpqRwOC49afAcAkuCy/hqZq5l1ECcMbHIcGztQ/yNpZvMTsUfvlJ4MDhJD7fw+04hgroK1F0/b+RPKvJ+qTAYVihQ4QoskK2fa9szUYizNDAYc+f6WaFEY5q2PbOUzUYhOHylMKLhm/ZhHYK7WECSwc41Zombp64zUVgaVCLRchSfBIYYCEDaxMoQCzbvPfnXPBQmZ6HA4+Mc7OIHjx8ES9w2xVi8LZ67zkQh7t0IqY9X//aBcrGcX4ayPj73nIdClwHC+pPo+JbmtteocI6eIRZiucHSou459TUPhYxvXChT73C4IMDx2ZG8GFK4WCiso2Y9D5iLQufuYBRz3K5eLsxl21/oosJCaqfREOlDzxNmojDiufWhdYXnSniX4c6MCYqTi69zrPJLmK99J1HmoRBwFTVZZ6U/4lTuu9O+8mW1PcSsgVS669uhmYdClJFI5wjbxf7juLM/1NUFYHsgTUx53zboPBQ6iflPVxAW4ljd53/lVqPJAa9R8b4TbDNRiH6QNRCXhWAxzfl49QpRvhNodKQITM56z7rPRSG8acKO8cWCGrNYNYfmWJxbrN8oPBfVDfSej0KsqB1iqbyjF6KtXdHUe34l9kP7wPNR6DYpcucI8eiB2zi8KjTnsv+oCZuTwkvRbauNkRjOeIGo0ASNG8C5z1IEx6lcBRpm5WXz1xqjwLb2FLFufeal0P1Uh1SqOK5hJdr39Slh0QuBs1PI3dYTS5anDtjjoW53XmriY8hvFeGvGrLec20viHxFWMXjnqC9VPWT199O+6VHg49xCge2DP4SoBBNfrx5dW7vV2HRwo1hMqbCyH9fRGyrJPpmkqRq3EEGPfLpy8YoIQOh5PcTtlbaUKSjnr7krNJi6Jj373EpV9m4G/UkO/zfOzO0z/37Ei3EBRkfeZayKn36isG3KcTax2b87z4lb37v/ZsFwgOF0cVzrf/vA2nDW7b4E6S7cb9rccXNoiiKvtkbutPv4DTGlsd8TP2HjAEf18oQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQxJ/jP0T3d4FVZMxuAAAAAElFTkSuQmCC" width="250" height="180"/>
                        <h6>Preview</h6>

                        <?php
                            echo $this->Form->input('image', array(
                                'label' => false,
                                'class' => 'form-control',
                                'type' => 'file',
                            ));
                        ?>
                    </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Full Name:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->input('name', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Username:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->input('username', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Password:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->input('password', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'password',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Email Address:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->input('email', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'email',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Location:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->input('location', array(
                        'label' => false,
                        'class' => 'form-control',
                        'type' => 'text',
                    ));
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Birthday:</label>
                <div class="col-lg-8">
                <?php
                    // echo $this->Form->input('birthday', array(
                    //     'label' => false,
                    //     'class' => 'form-control',
                    //     'type' => 'date'
                    // ));

                    echo $this->Form->input('birthday' ,['label' => false, 'class' => 'form-control']);
                ?>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Description:</label>
                <div class="col-lg-8">
                <?php
                    echo $this->Form->input('description', array(
                    'label' => false,
                    'type' => 'text',
                    'text' => '',
                    'rows' => '4',
                    'maxlength' => '140',
                    'class' => 'form-control',
                    'placeholder' => 'Please enter up to 140 characters',
                    'style' => 'margin-top: 10px'));
                ?>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8">
                <?= $this->Form->input('Save',[
                    'type' => 'button',
                    'label' => false,
                    'class'=>'btn btn-primary float-right'
                    ]);?>
                <?= $this->Form->end() ?>
                <span></span>
                <button class="btn btn-secondary"><a href="<?php echo $url ?>users/profile">Cancel </a></button>
            </div>
        </div>
      </div>
  </div>
</div>
<hr>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

        $("#UserImage").change(function() {
        readURL(this);
    });
</script>