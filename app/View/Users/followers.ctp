<div class="container">
    <div class="container-fluid gedf-wrapper">
        <div class="row">
            <div class="offset-1 col-md-10 gedf-main">
                <!-- Main Tweet Section-->
                    <div class="card gedf-card">
                        <div class="card-header">
                            <h4> List of followers </h4>
                            <p style="text-align: right">
                            <?php echo $this->Html->Link('Return', array(
                               'label' => false,
                                'class' => 'btn btn-outline-secondary',
                                'role' => 'button',
                                'action' => 'dashboard'
                                   )
                                );
                        ?></p>
                            <hr>
                        </div>
                        <?php foreach ($followingUser as $f):?>
                            <div>
                                <div class="card border-light bg-white card proviewcard shadow-sm offset-1 col-md-9 offset-1">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <div class="col-lg-12 p-3 cardlist">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2">
                                                            <div class="row">
                                                            <?= $this->Html->image("/img/users/" . $f['Follows']['image'], ['class' => '', 'width' => '70', 'height' => '80']);?>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-lg-9 col-xl-10">
                                                            <div class="d-block text-truncate mb-1">
                                                                <?php $id = $f['Follows']['id'] ?>
                                                                <?php echo $this->Html->Link($f['Follows']['username'], array(
                                                                'label' => false,
                                                                'role' => 'button',
                                                                'action' => 'other_user', $id
                                                                    )
                                                                );
                                                                ?>
                                                            </div>
                                                            <div class="location d-block">
                                                                <span><?php echo $f['Follows']['email']; ?> </span>
                                                            </div>
                                                            <div class="cartviewprice d-block">
                                                                <span>Location: <?php echo $f['Follows']['location']; ?> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2 p-0 qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 ml-lg-auto align-self-start mt-2 mt-lg-0">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <div class="card gedf-card">
                        <div class="card-header">
                            <h4> List of people you followed </h4>
                            <hr>
                        </div>
                        <?php foreach ($followUser as $ff):?>
                            <div>
                                <div class="card border-light bg-white card proviewcard shadow-sm offset-1 col-md-9 offset-1">
                                <div class="card-header"></div>
                                <div class="card-body">
                                    <div class="col-lg-12 p-3 cardlist">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2">
                                                            <div class="row">
                                                            <?= $this->Html->image("/img/users/" . $ff['User']['image'], ['class' => '', 'width' => '70', 'height' => '80']);?>
                                                            </div>
                                                        </div>
                                                        <div class="col-8 col-lg-9 col-xl-10">
                                                            <div class="d-block text-truncate mb-1">
                                                                <?php $id = $ff['User']['id'] ?>
                                                                <?php echo $this->Html->Link($ff['User']['username'], array(
                                                                'label' => false,
                                                                'role' => 'button',
                                                                'action' => 'other_user', $id
                                                                    )
                                                                );
                                                                ?>
                                                            </div>
                                                            <div class="location d-block">
                                                                <span><?php echo h($ff['User']['email']); ?> </span>
                                                            </div>
                                                            <div class="cartviewprice d-block">
                                                                <span>Location: <?php echo h($ff['User']['location']); ?> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-4 col-lg-3 col-xl-2 p-0 qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 ml-lg-auto align-self-start mt-2 mt-lg-0">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!-- Main Tweet Section End-->
            </div>                
        </div>
    </div>
</div>

<script type="text/javascript">
</script>