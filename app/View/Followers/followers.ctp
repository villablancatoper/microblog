<div class="container">
    <div class="container-fluid gedf-wrapper">
        <div class="row">
            <div class="offset-1 col-md-10 gedf-main">
                <!-- Main Tweet Section-->
                    <div class="card gedf-card">
                        <div class="card-header">
                            <h4> List of followers </h4>
                            <p style="text-align: right">
                            <?php echo $this->Html->Link('Return', array(
                               'label' => false,
                                'class' => 'btn btn-outline-secondary',
                                'role' => 'button',
                                'action' => 'dashboard'
                                   )
                                );
                        ?></p>
                            <hr>
                        </div>
                        <?php //foreach ():?>
                            <div class="card-body">
                                <div class="card gedf-card"> 
                                            <div class="card-header">
                                                <div class="d-flex justify-content-between align-items-center">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div class="mr-2">
                                                                <img class="rounded-circle" width="45" src="<?php $t['User']['image']; ?>" alt="">
                                                            </div>
                                                            <div class="ml-2">
                                                                <div class="h5 m-0"><?php echo $t['Retweeter']['username']; ?> retweeted: </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        </div>
                                                    </div>
                                </div>
                            </div>
                        <?php //endforeach ?>
                    </div>
                <!-- Main Tweet Section End-->
            </div>                
        </div>
    </div>
</div>

<script type="text/javascript">
</script>