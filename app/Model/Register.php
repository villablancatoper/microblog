<?php
App::uses('AppModel', 'Model');

    class Register extends AppModel {
        public $validate = array(
            'username' => array(
                'required' => array(
                    'rule' => 'notBlank',
                    'message' => 'Username is Required, Please enter a Username'
                ),
                'alphaNumeric' => array(
                    'rule' => 'alphaNumeric',
                    'required' => 'true',
                    'message' => 'Username must contain numbers and letters'
                ),
                'minLength' => array(
                    'rule' => array('minLength', '5'),
                    'message' => 'Username must be 5 characters or more'
                )
            ),
            'cpassword' => array(
                'rule' => array('cpassword'),
                'message' => 'Confirmed password must be the same as Repeat password',
            ),
            'password' => array(
                'required' => array(
                    'rule' => 'notBlank',
                    'message' => 'Password is Required, Please enter a Password'
                ),
                'minLength' => array(
                    'rule' => array('minLength', '5'),
                    'message' => 'Password must 5 characters or more'
                )
            ),
            'fullname' => array(
                'required' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please enter your Full name'
                )
            ),
            'email' => array(
                'required' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please enter an Email Address'
                )
            ),
            'location' => array(
                'required' => array(
                    'rule' => 'notBlank',
                    'message' => 'Please enter your City'
                )
            ),
            'birthday' => array(
                'required' => array(
                    'rule' => 'notBlank',
                    'message' => 'please enter your Birthday'
                )
            )
        );
    }