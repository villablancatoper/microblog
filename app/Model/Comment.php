<?php

App::uses('AppModel', 'Model');

class Comment extends AppModel {
    public $actsAs = array('Containable');
    public $belongsTo = array('Tweet', 'User');
}