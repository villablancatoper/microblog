<?php

App::uses('AppModel', 'Model');

class Follower extends AppModel {
    // public $actsAs = array('Containable');
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'following_id'
        ),
        'Follows' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
}