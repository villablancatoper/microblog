<?php

App::uses('AppModel', 'Model');

class Tweet extends AppModel {
    public $actsAs = array('Containable');
    public $hasMany = array('Comment');

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Retweeter' => array(
            'className' => 'User',
            'foreignKey' => 'retweet_user_id'
        ),
        'Like' => array(
            'className' => 'Like',
            'foreignKey' => 'user_id'
        )
    );
}