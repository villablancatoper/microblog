<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    public $actsAs = array('Containable');

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Username is Required, Please enter a Username'
            ),
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'required' => 'true',
                'message' => 'Username must contain numbers and letters'
            ),
            'minLength' => array(
                'rule' => array('minLength', '5'),
                'message' => 'Username must be 5 characters or more'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Password is Required, Please enter a Password'
            ),
            'minLength' => array(
                'rule' => array('minLength', '5'),
                'message' => 'Password must 5 characters or more'
            )
        ),
        'name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter your Full name'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter a valid email address'
            )
        ),
        'location' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter your City'
            )
        ),
        'birthday' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'please enter your Birthday'
            )
        )
    );


    public function beforeFilter() {
        $this->Auth->allow('login', 'view');
    }

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
}